#include "globals.h"

#include <QLabel>
#include <QHBoxLayout>

namespace{

class HelloWorldLabel : public QWidget{
public:
	HelloWorldLabel() : QWidget(){
		QHBoxLayout* h=new QHBoxLayout;

		h->addWidget(new QLabel("1"));
		h->addWidget(new QLabel("2"));
		setLayout(h);

	}
};

INSTALL_TAB(HelloWorldLabel, __FILE__);

}
