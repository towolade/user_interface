#include <QtWidgets>

int main(int argc, char* argv[]){
  QApplication app(argc, argv);

  QPushButton* widget = new QPushButton("Click here!");
  QObject::connect(widget, SIGNAL(clicked()), &app, SLOT(quit()));
  widget->show();

  return app.exec();
}
