#include <QObject>
#include <QWidget>
#include <QLineEdit>

class LukeSearchWidget : public QWidget {
  Q_OBJECT
  public:
    explicit LukeSearchWidget(QWidget* parent = 0);
  public:
    void PageLayout();
  public slots:
    void Search();
  private:
    QLineEdit* searchField;
};
