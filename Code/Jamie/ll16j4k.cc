#include "globals.h"

#include <QLabel>
#include <QVBoxLayout>
#include <QtCore>
#include "gitpp7.h"
#include <QPalette>

namespace{

class ListCommitsLabel : public QWidget{
public:
	ListCommitsLabel() : QWidget(){

    QVBoxLayout* h=new QVBoxLayout;

		h->addWidget(new QLabel("<h3>This is the 10 most recent commits:</h3>"));

    std::string path=".";
    GITPP::REPO r(path.c_str());
    int counter = 0;

    for(auto i : r.commits()){

    //DEBUGGING
    //std::cout << i << " " << i.signature().name() << "TEST" << i.message() << "\n";

    int listNumber = counter + 1;
      QString commitList = QString("%3.Name of Author: %1 \nCommit Message: %2\n *******************\n").arg(i.signature().name().c_str()).arg(i.message().c_str()).arg(listNumber);
      QLabel *commit = new QLabel(commitList);
      h->addWidget(commit);
      counter++;

      if(counter > 9) {
        break;
      }
    }

    // QPalette pal = palette();
    //
    // pal.setColor(QPalette::Background, Qt::black);
    // ListCommitsLabel->setAutoFillBackground(true);
    // ListCommitsLabel->setPalette(pal);
    // ListCommitsLabel->show();

		setLayout(h);

	}
};

INSTALL_TAB(ListCommitsLabel, __FILE__);

}
