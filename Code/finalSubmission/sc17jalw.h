#pragma once
#include <QtWidgets>
#include <QObject>

namespace sc17jalw{
class RadioButtons : public QGroupBox{
	Q_OBJECT
public:
	explicit RadioButtons();
};

class BranchesWidget : public QWidget{
	Q_OBJECT

public:
	explicit BranchesWidget(QWidget *parent = 0);
	// ~BranchesWidget();

public:
  void ui();

public slots:
	void handleButton();

private:
	QString* branchSelected;
	RadioButtons* radioButtons;
};
}
