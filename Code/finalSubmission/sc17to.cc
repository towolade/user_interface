#include "globals.h"
#include "gitpp7.h"
#include <QLabel>
#include <QString>
#include <string>
#include <QHBoxLayout>
#include <QtCore>
#include <iostream>
#include <array>
#include <vector>

namespace{
class StatisticLabel : public QWidget
{
public:
	StatisticLabel() : QWidget(){
		QVBoxLayout* h=new QVBoxLayout;

		try {
			std::string path=".";
			GITPP::REPO r(path.c_str());

			// Author of Last Commit
			int counterAuth = 0;
			for(auto i: r.commits())
			{
				counterAuth++;
			}

			QString commitsArray[counterAuth];
			int counter = 0;


			// Last Commit
			h->addWidget(new QLabel("<h3> Last Commit: </h3>"));


			for(auto i: r.commits())
			{
				commitsArray[counter] = {i.message().c_str()};
				if(counter == counterAuth - 1) {
					QString* Qlastcommit = new QString(commitsArray[counter]);
					std::string lastc = Qlastcommit->toUtf8().constData();
					QLabel *lcommit = new QLabel(lastc.c_str());
					h->addWidget(lcommit);
				}
				counter++;
			}

			h->addWidget(new QLabel("<h3>By: </h3>"));



			counter = 0;
			for(auto i: r.commits())
			 {
				commitsArray[counter] = {i.signature().name().c_str()};
				if(counter == counterAuth - 1) {
					QString* Qlast = new QString(commitsArray[counter]);
					std::string last = Qlast->toUtf8().constData();
					QLabel *User = new QLabel(last.c_str());
					h->addWidget(User);
				}
				counter++;
			}


			// Total number of commits
			h->addWidget(new QLabel("<h3>Total Number Of Commits: </h3>"));



			QString NumCommits = QString::number(counterAuth);
			QLabel *totCommits = new QLabel(NumCommits);
			h->addWidget(totCommits);

			// Total number of Braunches

			int branchescount = 0;
			h->addWidget(new QLabel("<h3>Total Number Of Branches: </h3>"));

			for(auto i: r.branches())
			{
				branchescount++;
			}

			QString NumBranches = QString::number(branchescount);
			QLabel *totBraunches = new QLabel(NumBranches);
			h->addWidget(totBraunches);
		} catch (GITPP::EXCEPTION_CANT_FIND const &) {
			QLabel* noRepo = new QLabel("No repository present.");
			h->addWidget(noRepo);
		}


		setLayout(h);
	}

	// Braunches


};
INSTALL_TAB(StatisticLabel, __FILE__);
}
