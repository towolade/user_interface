#include "globals.h"
#include "gitpp7.h"
#include "sc17jalw.h"

#include <QDebug>
#include <QtWidgets>

namespace sc17jalw{

RadioButtons::RadioButtons() : QGroupBox(){
	try {
		std::string path = ".";
		GITPP::REPO r(path.c_str());

		// Get selected branch name.
		QVBoxLayout *vbox = new QVBoxLayout;
		// For every branch on the repository,
		for (auto branch : r.branches()){
			vbox->addWidget(new QRadioButton(tr(branch.name().c_str())));
		}
		vbox->addStretch(1);
		setLayout(vbox);
	} catch (GITPP::EXCEPTION_CANT_FIND const &){
		QVBoxLayout *vbox = new QVBoxLayout;
		QLabel* repoNotFound = new QLabel("There is no repository.");
		vbox->addWidget(repoNotFound);
		vbox->addStretch(1);
		setLayout(vbox);
	}

}

BranchesWidget::BranchesWidget(QWidget *parent) : QWidget(parent){
	ui();
}

void BranchesWidget::handleButton(){

	try {
		std::string path = ".";
		GITPP::REPO r(path.c_str());

		// Find which button is selected.
		std::string newBranch;
		QList<QRadioButton *> buttons = radioButtons->findChildren<QRadioButton *>();
		for (auto i : buttons){
			if (i->isChecked() == true){
				// qDebug() << i->text();
				newBranch = i->text().toUtf8().constData();
			}
		}

		// Checkout to new branch.
		r.checkout(newBranch);
	} catch (GITPP::EXCEPTION_CANT_FIND const &){
		QLabel* repoNotFound = new QLabel("There is no repository.");
		layout()->addWidget(repoNotFound);
	}
}

void BranchesWidget::ui(){
	QVBoxLayout* h=new QVBoxLayout;

	radioButtons = new RadioButtons();
	h->addWidget(radioButtons);

	QPushButton* changeBranchButton = new QPushButton("\nChange Branch\n");
	QObject::connect(changeBranchButton, SIGNAL(clicked()), this, SLOT(handleButton()));
	h->addWidget(changeBranchButton);

	setLayout(h);
}


INSTALL_TAB(BranchesWidget, __FILE__);
}
