#include "globals.h"
#include <QtWidgets>
#include "gitpp7.h"
#include "sc17lm.h"
#include <QDebug>

namespace sc17lm{
LukeSearchWidget::LukeSearchWidget(QWidget* parent) : QWidget(parent) {
	PageLayout();
}

void LukeSearchWidget::Search() {

	try {
		QString userInput = searchField->text();
		std::string path=".";
		GITPP::REPO r(path.c_str());
		std::string standardUserInput = userInput.toUtf8().constData();

		for(auto i : r.commits()) {
			std::string commitMessage = i.message().c_str();
			if(commitMessage.find(standardUserInput) != std::string::npos) {
				QString commitFound = QString("%1 - %2").arg(i.signature().name().c_str()).arg(i.message().c_str());
				QLabel* listCommitFound = new QLabel(commitFound);
				QLabel* divider = new QLabel("*******************");
				QString title = "Results for : " + userInput;
				QLabel* titleLabel = new QLabel(title);
				layout()->addWidget(titleLabel);
				layout()->addWidget(listCommitFound);
				layout()->addWidget(divider);
			}
		}
	} catch (GITPP::EXCEPTION_CANT_FIND const &) {
			QLabel* repoNotFound = new QLabel("There is no repository.");
			layout()->addWidget(repoNotFound);
	}

};

void LukeSearchWidget::PageLayout() {
	//creating the new widget called LukeSearchWidget.
		//Instances of Qt elements.
		QVBoxLayout* layout = new QVBoxLayout;
		QLabel* titleLbl = new QLabel("Search through repository commits");
	  QLabel* searchLbl = new QLabel("Keyword: ");
	  searchField = new QLineEdit();
	  QPushButton* searchBtn = new QPushButton("Search");
		//adding the instances of the elements to the instance of the layout.
		layout->addWidget(titleLbl);
		layout->addStretch();
	  layout->addWidget(searchLbl);
	  layout->addWidget(searchField);
	  layout->addStretch();
	  layout->addWidget(searchBtn);
		QObject::connect(searchBtn, SIGNAL(clicked()), this, SLOT(Search()));
		setLayout(layout);
};

//installing the tab to the program.
INSTALL_TAB(LukeSearchWidget, __FILE__);
}
