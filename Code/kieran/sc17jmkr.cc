#include "globals.h"
#include "gitpp7.h"
#include <QLabel>
#include <QHBoxLayout>
#include <QPushButton>
#include <QObject>


namespace{

	bool CheckGitRepo() {
	    std::string path = ".";
	    try {
	        GITPP::REPO r(path.c_str());
	        return (1);
	    } catch (GITPP::EXCEPTION_CANT_FIND const &) {
	        return (0);
	    }
	};

class sc17jmkrL : public QWidget{
	Q_OBJECT
public slots:
	void createrepo(){
		std::string path = ".";
		GITPP::REPO r(GITPP::REPO::_create, path.c_str());
	}
public:
	sc17jmkrL() : QWidget(){
		QPushButton* bob=new QPushButton;
		QHBoxLayout* h=new QHBoxLayout;
		bool RepoCheck = CheckGitRepo();
				if (RepoCheck) {
						h->addWidget(new QLabel("There is a git repo"));
				}
				else {
						h->addWidget(new QLabel("There is no git repo"));
				}
		h->addWidget(bob);
		QObject::connect(bob, SIGNAL(clicked()), this, sc17jmkrL::SLOT(createrepo()));
		setLayout(h);

	}

};

INSTALL_TAB(sc17jmkrL, __FILE__);

}
