#include "globals.h"
#include "gitpp7.h"
#include "sc17jmkr.h"
#include <QLabel>
#include <QHBoxLayout>
#include <QPushButton>
#include <QObject>
#include <QDebug>

	bool CheckGitRepo() {
	    std::string path = ".";
	    try {
	        GITPP::REPO r(path.c_str());
	        return (1);
	    } catch (GITPP::EXCEPTION_CANT_FIND const &) {
	        return (0);
	    }
	};

	sc17jmkrL::sc17jmkrL(QWidget *parent) : QWidget(parent) {
     userinterface();
	 }
	void sc17jmkrL::createrepo(){
		std::string path = ".";
		GITPP::REPO r(GITPP::REPO::_create, path.c_str());
	}

	void sc17jmkrL::userinterface(){
			QPushButton* bob=new QPushButton;
			bob->setMinimumSize(240,60);
			QHBoxLayout* h=new QHBoxLayout;
			bool RepoCheck = CheckGitRepo();
					if (RepoCheck) {
							h->addWidget(new QLabel("There is a git repo"));
					}
					else {
							h->addWidget(new QLabel("There is no git repo"));
							QObject::connect(bob, SIGNAL(clicked()), this, SLOT(createrepo()));
					}
			h->addWidget(bob);
			setLayout(h);
		};

	INSTALL_TAB(sc17jmkrL, __FILE__);
